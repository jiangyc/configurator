/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator;

import com.jiangyc.configurator.util.Paths;
import org.junit.jupiter.api.Test;

class PathsTest {

    @Test
    void getUserHome() {
        System.out.println("User Home: " + Paths.getUserHome());
    }

    @Test
    void getUserConfig() {
        System.out.println("User Config: " + Paths.getUserConfig());
    }
}