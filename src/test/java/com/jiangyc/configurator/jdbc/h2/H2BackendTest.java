/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.jdbc.h2;

import com.jiangyc.configurator.core.Backend;
import com.jiangyc.configurator.core.Configuration;
import com.jiangyc.configurator.core.Configurator;
import com.jiangyc.configurator.core.Configurators;
import org.junit.jupiter.api.Test;

public class H2BackendTest {

    @Test
    public void test() {
        // initialize
        Backend backend = new H2Backend();
        Configurators.initialize(backend);
        Configurator configurator = Configurators.get();

        System.out.println("=============== test: add test1 ===============");
        configurator.set(new Configuration("test1", "test1_value", "test1_description"));
        System.out.println(configurator.get("test1"));

        System.out.println("=============== test: set test1 ===============");
        configurator.set(new Configuration("test1", "test1_value_2", "test1_description_2"));
        System.out.println(configurator.get("test1"));

        System.out.println("=============== test: del test1 ===============");
        configurator.del("test1");
        System.out.println(configurator.get("test1"));
    }
}
