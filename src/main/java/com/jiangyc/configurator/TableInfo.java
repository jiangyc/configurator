/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator;

import cn.hutool.db.Entity;
import com.jiangyc.configurator.core.Mapper;

import java.util.List;

/**
 * 数据库表信息
 *
 * @author 姜永春
 */
public interface TableInfo<T> extends Mapper<T, Entity> {

    /**
     * 获取建表语句
     *
     * @return 建表语句
     */
    String getCreatetableStatement();

    /**
     * 获取表名
     *
     * @return 表名
     */
    String getTableName();

    /**
     * 获取Java类的字段列表
     *
     * @return Java类的字段列表
     */
    List<String> getFields();

    /**
     * 根据Java类的字段名，获取数据表的表名
     *
     * @param fieldName Java类的字段名
     * @return 数据表的表名
     */
    String getColumnName(String fieldName);
}
