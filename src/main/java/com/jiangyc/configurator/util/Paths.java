/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/**
 * 文件路径工具类
 *
 * @author 姜永春
 */
public class Paths {

    /**
     * 获取用户主目录或主目录下文件的路径
     *
     * @param subPath 子路径，如果该子路径存在，则为获取用户主目录下子文件的路径
     * @return 用户主目录或主目录下文件的路径
     */
    public static Path getUserHome(String... subPath) {
        return Path.of(System.getProperty("user.home"), subPath);
    }

    /**
     * 获取用户配置目录或配置目录下文件的路径
     *
     * @param subPath 子路径，如果该子路径存在，则为获取用户配置目录下子文件的路径
     * @return 用户配置目录或配置目录下文件的路径
     */
    public static Path getUserConfig(String... subPath) {
        return Path.of(getUserHome(".config").toString(), subPath);
    }

    /**
     * 获取应用配置目录或应用配置目录下文件的路径
     * @param appName 应用名称，如果存在，则会将其作为用户配置目录的第一个子路径
     * @param subPath 子路径，如果该子路径存在，则为获取应用配置目录下子文件的路径
     * @return 应用配置目录或应用配置目录下文件的路径
     */
    public static Path getAppConfig(String appName, String... subPath) {
        return (appName == null || appName.isBlank()) ? getUserConfig(subPath)
                : Path.of(getUserConfig(appName).toString(), subPath);
    }

    /**
     * 确保给定的路径为有效的目录路径，如果不存在，则新建
     *
     * @param path 路径
     */
    public static void mkdirs(Path path) {
        Objects.requireNonNull(path, "Path can not be null");
        if (Files.exists(path) && Files.isDirectory(path)) {
            return;
        }

        if (Files.exists(path) && !Files.isDirectory(path)) {
            throw new IllegalArgumentException("The path exists, and it is not a directory: " + path);
        }

        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 确保给定的路径的父路径为有效的目录路径，如果不存在，则新建
     *
     * @param path 路径
     */
    public static void mkdirsParent(Path path) {
        mkdirs(path.getParent());
    }
}
