/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.core;

/**
 * 类型转换器，将一种类型的对象转换为另一种类型的对象
 *
 * @author 姜永春
 * @param <T> 转换后的对象的类型
 * @param <E> 转换前的对象的类型
 */
public interface Mapper<T, E> {

    /**
     * 将一种类型的对象转换为另一种类型的对象
     *
     * @param e 转换前的对象
     * @return 转换后的对象
     * @throws Exception 当转换失败时，抛出此异常
     */
    T map(E e) throws Exception;
}
