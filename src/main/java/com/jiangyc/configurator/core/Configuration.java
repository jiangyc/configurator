/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.core;

import java.util.Objects;

/**
 * 配置项
 *
 * @author 姜永春
 */
public class Configuration {

    /**
     * 配置项名称
     */
    private String name;

    /**
     * 配置项值
     */
    private String value;

    /**
     * 配置项描述
     */
    private String description;

    /**
     * 默认的构造方法，构造一个空的配置项
     */
    public Configuration() {
    }

    /**
     * 使用给定的配置项名称和值构造配置项
     *
     * @param name 配置项名称
     * @param value 配置项值
     */
    public Configuration(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * 使用给定的配置项名称、值和描述构造配置项
     * @param name 配置项名称
     * @param value 配置项值
     * @param description 配置项描述
     */
    public Configuration(String name, String value, String description) {
        this.name = name;
        this.value = value;
        this.description = description;
    }

    /**
     * 获取配置项名称
     *
     * @return 配置项名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置配置项名称
     *
     * @param name 要设置的配置项名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取配置项值
     *
     * @return 配置项值
     */
    public String getValue() {
        return value;
    }

    /**
     * 设置配置项值
     *
     * @param value 要设置的配置项值
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 获取配置项描述
     *
     * @return 配置项描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 将该配置项的字符串值用给定的类型转换器转换为另一种类型
     *
     * @param mapper 转换字符串值的类型转换器
     * @return 转换后的配置项值
     * @param <T> 转换后的值的类型
     */
    public <T> T toValue(Mapper<T, String> mapper){
        try {
            return mapper.map(value);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 设置配置项描述
     *
     * @param description 要设置的配置项描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Configuration that = (Configuration) o;
        return name.equals(that.name) && value.equals(that.value) && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value, description);
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
