/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.core;

/**
 * 配置器的后端接口
 *
 * @author 姜永春
 */
public interface Backend {

    /**
     * 初始化配置器后端
     *
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    void initialize();

    /**
     * 判断该配置器后端是否可读
     *
     * @return 该配置器后端是否可读
     */
    boolean isReadable();

    /**
     * 判断给定的配置项名称是否存在
     *
     * @param name 配置项名称
     * @return 给定的配置项名称是否存在
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    boolean has(String name);

    /**
     * 根据给定的配置项名称来获取配置项
     *
     * @param name 配置项名称
     * @return 配置项
     * @throws RuntimeException 获取配置项出错时，抛出此异常
     */
    Configuration get(String name);

    /**
     * 判断该配置器后端是否可写
     *
     * @return 该配置器后端是否可读
     */
    boolean isWritable();

    /**
     * 设置配置
     *
     * @param cfg 配置项
     * @return 是否设置成功
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    boolean set(Configuration cfg);

    /**
     * 删除配置
     *
     * @param name 配置项名称
     * @return 删除配置项是否成功
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    boolean del(String name);

    /**
     * 清空配置
     *
     * @return 删除配置项是否成功
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    boolean cls();
}
