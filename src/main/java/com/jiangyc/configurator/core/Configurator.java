/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.core;

import com.jiangyc.configurator.core.Backend;
import com.jiangyc.configurator.core.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 配置管理器
 *
 * @author 姜永春
 */
public class Configurator {

    /**
     * 配置器后端
     */
    private final Backend backend;
    /**
     * 配置缓存项
     */
    private final Map<String, Configuration> cfgMap = new HashMap<>();

    /**
     * 默认的构造方法，根据给定的配置器后端初始化配置器
     *
     * @param backend 配置器
     * @throws RuntimeException 初始化失败时抛出此异常
     */
    Configurator(Backend backend) {
        Objects.requireNonNull(backend, "Backend can not be null");

        this.backend = backend;
        backend.initialize();
    }

    /**
     * 判断该配置器后端是否可读
     *
     * @return 该配置器后端是否可读
     */
    public synchronized boolean isReadable() {
        return backend.isReadable();
    }

    /**
     * 判断给定的配置项名称是否存在
     *
     * @param name 配置项名称
     * @return 给定的配置项名称是否存在
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    public synchronized boolean has(String name){
        Objects.requireNonNull(name, "Configuration name can not be null");
        return isReadable() && (cfgMap.containsKey(name) || backend.has(name));
    }

    /**
     * 根据给定的配置项名称来获取配置项
     *
     * @param name 配置项名称
     * @return 配置项
     * @throws RuntimeException 获取配置项出错时，抛出此异常
     */
    public synchronized Configuration get(String name) {
        Objects.requireNonNull(name, "Configuration name can not be null");

        if (isReadable()) {
            Configuration cfg = cfgMap.get(name);

            if (cfg == null) {
                cfg = backend.get(name);
                if (cfg != null) {
                    cfgMap.put(name, cfg);
                }
            }

            return cfg;
        }
        return null;
    }

    /**
     * 判断该配置器后端是否可写
     *
     * @return 该配置器后端是否可写
     */
    public synchronized boolean isWritable() {
        return backend.isWritable();
    }

    /**
     * 设置配置
     *
     * @param cfg 配置项
     * @return 是否设置成功
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    public synchronized boolean set(Configuration cfg) {
        Objects.requireNonNull(cfg, "Configuration can not be null");
        Objects.requireNonNull(cfg.getName(), "Configuration name can not be null");

        if (isWritable()) {
            if (backend.set(cfg)) {
                cfgMap.put(cfg.getName(), cfg);
                return true;
            }
        }
        return false;
    }

    /**
     * 删除配置
     *
     * @param name 配置项名称
     * @return 删除配置项是否成功
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    public synchronized boolean del(String name) {
        Objects.requireNonNull(name, "Configuration name can not be null");

        if (isWritable()) {
            if (backend.del(name)) {
                cfgMap.remove(name);
                return true;
            }
        }
        return false;
    }

    /**
     * 清空配置
     *
     * @return 删除配置项是否成功
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    public synchronized boolean cls() {
        if (isWritable()) {
            if (backend.cls()) {
                cfgMap.clear();
                return true;
            }
        }
        return false;
    }
}
