/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 配置管理器工具类
 *
 * @author 姜永春
 */
public class Configurators {
    /** 缓存已有的配置器的集合 */
    private static final Map<String, Configurator> CONFIGURATOR_MAP = new HashMap<>();
    /**
     * 默认配置器名称
     */
    private static final String DEFAULT_CONFIGURATOR_NAME = "default";

    /**
     * 初始化配置
     *
     * @param backend 配置器后端
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    public static void initialize(Backend backend) {
        initialize(backend, false);
    }

    /**
     * 初始化配置
     *
     * @param backend 配置器后端
     * @param cover 是否重新初始化
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    public static void initialize(Backend backend, boolean cover) {
        initialize(null, backend, cover);
    }

    /**
     * 初始化配置
     *
     * @param name 配置器后端名称，如无，则默认为"default"
     * @param backend 配置器后端
     * @param cover 是否重新初始化
     * @throws RuntimeException 执行出错时，抛出此异常
     */
    public static synchronized void initialize(String name, Backend backend, boolean cover) {
        Objects.requireNonNull(backend, "Backend can not be null");

        String configuratorName = name == null || name.isBlank() ? DEFAULT_CONFIGURATOR_NAME : name;
        Configurator configurator = CONFIGURATOR_MAP.get(configuratorName);
        if (configurator != null && !cover) {
            return;
        }

        configurator = new Configurator(backend);
        CONFIGURATOR_MAP.put(configuratorName, configurator);
    }

    /**
     * 获取默认的配置器
     *
     * @return 默认的配置器
     */
    public static Configurator get() {
        return get(null);
    }

    /**
     * 根据配置器名称获取配置器
     *
     * @param name 配置器名称
     * @return 配置器
     */
    public static synchronized Configurator get(String name) {
        return name == null || name.isBlank() ? CONFIGURATOR_MAP.get(DEFAULT_CONFIGURATOR_NAME) : CONFIGURATOR_MAP.get(name);
    }
}
