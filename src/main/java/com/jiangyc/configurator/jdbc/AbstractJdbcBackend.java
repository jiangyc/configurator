/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.jdbc;

import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import com.jiangyc.configurator.TableInfo;
import com.jiangyc.configurator.core.Configuration;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author jiangyc
 */
public abstract class AbstractJdbcBackend implements JdbcBackend, TableInfo<Configuration> {

    /**
     * hutool的数据库工具类
     */
    protected Db db;
    /**
     * 用户指定的数据源
     */
    protected DataSource userDataSource;
    /**
     * 实体类字段名
     */
    protected List<String> fields = List.of("name", "value", "description");
    /**
     * 数据表的列的集合
     */
    protected Map<String, String> fieldColumnMap = Map.of("name", "cfg_name", "value", "cfg_value", "description", "cfg_description");

    /**
     * 默认的构造方法，初始化hutool的数据库工具类
     */
    public AbstractJdbcBackend() {
        this(null);
    }

    /**
     * 使用给定的数据源初始化
     *
     * @param ds 配置器数据源
     */
    public AbstractJdbcBackend(DataSource ds) {
        this.userDataSource = ds;
        this.db = Db.use(getDataSource());
    }

    @Override
    public void initialize() {
        try {
            db.execute(getCreatetableStatement());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isReadable() {
        return true;
    }

    @Override
    public boolean has(String name) {
        try {
            return db.count(new Entity(getTableName()).set(getColumnName("name"), name)) > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Configuration get(String name) {
        try {
            return map(db.get(new Entity(getTableName()).set(getColumnName("name"), name)));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isWritable() {
        boolean isWritable;

        try(Connection conn = Db.use(getDataSource()).getConnection()) {
            isWritable = !conn.isReadOnly();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return isWritable;
    }

    @Override
    public boolean set(Configuration cfg) {
        Entity entity = new Entity(getTableName())
                .set(getColumnName("name"), cfg.getName())
                .set(getColumnName("value"), cfg.getValue())
                .set(getColumnName("description"), cfg.getDescription());

        try {
            if (has(cfg.getName())) {
                return db.update(entity, new Entity(getTableName()).set(getColumnName("name"), cfg.getName())) > 0;
            } else {
                return db.insert(entity) > 0;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean del(String name) {
        try {
            return db.del(getTableName(), getColumnName("name"), name) > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean cls() {
        try {
            return db.execute("DELETE FROM " + getTableName() + " WHERE 1 = 1") > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DataSource getDataSource() {
        if (userDataSource != null) {
            return userDataSource;
        }

        return null;
    }

    @Override
    public List<String> getFields() {
        return fields;
    }

    @Override
    public String getColumnName(String fieldName) {
        Objects.requireNonNull(fieldName, "Field can not be null: " + fieldName);
        if (!fieldColumnMap.containsKey(fieldName)) {
            throw new IllegalArgumentException("Unknown field: " + fieldName);
        }
        return fieldColumnMap.get(fieldName);
    }

    @Override
    public Configuration map(Entity entity) {
        return entity == null ? null : new Configuration(entity.getStr(getColumnName("name")),
                entity.getStr(getColumnName("value")),
                entity.getStr(getColumnName("description")));
    }
}
