/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.jdbc.h2;

import cn.hutool.db.Db;
import cn.hutool.db.ds.simple.SimpleDataSource;

import com.jiangyc.configurator.jdbc.AbstractJdbcBackend;
import com.jiangyc.configurator.util.Paths;

import javax.sql.DataSource;
import java.nio.file.Path;

/**
 * 基于H2的配置器的后端
 *
 * @author 姜永春
 */
public class H2Backend extends AbstractJdbcBackend {
    /**
     * 用户指定的数据库文件路径
     */
    private Path userDbPath = null;

    /**
     * 默认的构造方法，初始化hutool的数据库工具类
     */
    public H2Backend() {
        super();
    }

    /**
     * 使用给定的数据源初始化
     *
     * @param ds 配置器数据源
     */
    public H2Backend(DataSource ds) {
        super(ds);
    }

    /**
     * 使用给定的数据库路径初始化
     *
     * @param dbPath 初始化配置器数据源所使用的数据库的路径
     */
    public H2Backend(Path dbPath) {
        this.userDbPath = dbPath;
        this.db = Db.use(getDataSource());
    }

    @Override
    public DataSource getDataSource() {
        if (super.getDataSource() == null) {
            Path dbPath = userDbPath == null ? Paths.getAppConfig("configurator", "config") : userDbPath;
            Paths.mkdirsParent(dbPath);
            super.userDataSource = new SimpleDataSource("jdbc:sqlite:" + dbPath, "", "", "org.sqlite.JDBC");
        }

        return super.getDataSource();
    }

    @Override
    public String getCreatetableStatement() {
        return "CREATE TABLE IF NOT EXISTS configuration (cfg_name VARCHAR(200) PRIMARY KEY, cfg_value TEXT, cfg_description TEXT);";
    }

    @Override
    public String getTableName() {
        return "configuration";
    }
}
