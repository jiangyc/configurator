/*
 * configurator
 * Copyright (C) 2022 姜永春
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.jiangyc.configurator.jdbc;

import com.jiangyc.configurator.core.Backend;

import javax.sql.DataSource;

/**
 * 基于JDBC的配置器的后端接口
 *
 * @author 姜永春
 */
public interface JdbcBackend extends Backend {

    /**
     * 获取配置数据源
     *
     * @return 配置数据源
     */
    DataSource getDataSource();
}
