plugins {
    java
    `maven-publish`
    signing
}

group = "com.jiangyc"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {
    // runtimeOnly("org.xerial:sqlite-jdbc:3.36.0.3")
    // implementation("com.typesafe:config:1.4.2")
    implementation("cn.hutool:hutool-all:5.8.4")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")
    testRuntimeOnly("org.xerial:sqlite-jdbc:3.36.0.3")
    testRuntimeOnly("com.h2database:h2:2.1.214")
}

java {
    withSourcesJar()
    withJavadocJar()
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

tasks.compileJava {
    options.encoding = "UTF-8"
}

publishing {
    val sonatypeUsername = project.findProperty("sonatype.username") as String? ?: System.getenv("USERNAME")
    val sonatypePassword = project.findProperty("sonatype.password") as String? ?: System.getenv("PASSWORD")
    repositories {
        maven {
             url = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
//            url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots")
            credentials {
                username = sonatypeUsername
                password = sonatypePassword
            }
        }
    }
    publications {
        create<MavenPublication>("sonatype") {
            // groupId = "com.jiangyc"
            // artifactId = "reptile-core"
            // version = "1.0.1"
            from(components["java"])
            pom {
                name.set("Configurator")
                description.set("Java配置读取与设置。")
                url.set("https://www.jiangyc.com/configurator")
                licenses {
                    license {
                        name.set("GNU GENERAL PUBLIC LICENSE, Version 3")
                        url.set("https://www.gnu.org/licenses/gpl-3.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("jiangyc")
                        name.set("佳木流泉")
                        email.set("JSpringYC@gmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/jiangyc/configurator.git")
                    developerConnection.set("scm:git:git://gitlab.com/jiangyc/configurator.git")
                    url.set("https://www.jiangyc.com/configurator")
                }
        }
        }
    }
}

signing {
    useGpgCmd()
    sign(configurations.archives.get())
    sign(publishing.publications["sonatype"])
}